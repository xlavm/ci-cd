# Directly deploy to heroku

## GitLab CI/CD Config

Add variables of GitLab CI/CD

![gitlab-ci-cd-variables](images/gitlab-ci-cd-variables.png)

Include the `.env` variables

### Note: 

`HEROKU_APP_PRODUCCTION` is the name of app: `env--app`

`HEROKU_API_KEY` is obtained of:

![obtaining-heroku-api-key](images/obtaining-heroku-api-key.png)

