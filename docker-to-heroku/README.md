# docker-to-heroku

Deploy app with docker and gitlab registry to heroku

## Variables CI/CD

```BASH
  CONTAINER_GITLAB: registry.gitlab.com/<gitlab_user>/<repository_name>
  CONTAINER_TAG: latest
  CONTAINER_HEROKU: registry.heroku.com/<app_heroku_name>/web
  HEROKU_API_KEY: <heroku_api_key_for_user>
  HEROKU_USER: <email/user_for_heroku_login>
  APP_HEROKU:<app_heroku_name>
```

Siendo:

* `web` un nombre cualquiera

En GitLab:

![variables-ci-cd](images/variables-ci-cd.png)
