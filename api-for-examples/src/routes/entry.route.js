const express = require('express')
const cors = require('cors')
const router = express.Router()
const entryController = require('../controllers/entry.controller');

router.use(cors())

router.post('/', entryController.create);
router.put('/:id', entryController.update);
router.get('/', entryController.findAll);
router.get('/findIp/:ip', entryController.findIp);

module.exports = router