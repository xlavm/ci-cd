var request = require('supertest')
var assert = require('assert')
var app = require('../server')
var newData = require('./dataset/newDataEntry')
var existingData = require('./dataset/existingDataEntry')

const validID = existingData._id//"603d27cb61a653001531dad9"
const invalidID = "this.is-a-invalid-id"
const validIP = existingData.ip //"200.46.145.42"

describe('# create entrys in DB', function () {

  test('Entry Created Successfully!', function (done) {
    request(app)
      .post('/api/v1/entry/')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send(newData)
      .expect(200, done)
  })

  test('Entry Already Exists in the DB', function (done) {
    request(app)
      .post('/api/v1/entry/')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send(existingData)
      .expect(200, done)
  })
})

describe('# update entrys in DB', function () {

  test('Entry Update Successfully!', function (done) {
    request(app)
      .put(`/api/v1/entry/${validID}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send(newData)
      .expect(200, done)
  })

  test('There is no entry with that ID', function (done) {
    request(app)
      .put(`/api/v1/entry/${invalidID}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send(existingData)
      .expect(200, done)
  })

})

describe('# finders of entrys', function () {

  test('Find all entrys', function (done) {
    request(app)
      .get('/api/v1/entry/')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.notEqual(response, null)
        done();
      })
      .catch(err => done(err))
  })

  test('Find one entry for ip', function (done) {
    request(app)
      .get(`/api/v1/entry/findIp/${validIP}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert(response.body.ip, `${validIP}`)
        done();
      })
      .catch(err => done(err))
  })

})

describe('# other cases', function () {

  test('page doesn`t exist', function (done) {
    request(app)
      .post('/api/v1/entrys/')
      .set('Accept', 'application/json')
      .expect('Content-Type', /text\/html/)
      .send(null)
      .expect(404, done)
    done();
  })

})