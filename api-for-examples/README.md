# API FOR EXAMPLES

## Principal Commands

Install Dependencies
```
npm install
```
build api
```
npm build
```
Execute in PROD
```
npm start
```
Execute in DEV
```
npm run start:dev
```

test API
```
npm test
```

## Project Local Config
.env file 
```
PORT=3000
PATH_MONGO=mongodb+srv://manager-apis-back-user:dyldMQYT7HV*&HwEtRc7W!H@manager-apis-back-clust.z1hax.mongodb.net/manager-apis-back-db?retryWrites=true&w=majority
PATH_API_ENTRY=/api/v1/entry
```