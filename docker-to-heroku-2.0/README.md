# docker-to-heroku 2.0

Deploy app with docker into heroku

## Variables CI/CD

```BASH
  GITLAB_CONTAINER: registry.gitlab.com/<gitlab_user>/<repository_name>
  HEROKU_API_KEY: <heroku_api_key_for_user>
  HEROKU_PROD_APP: <app-prod name in heroku>
  HEROKU_PROD_CONTAINER: registry.heroku.com/<app-prod name in heroku>/web
  HEROKU_TEST_APP: <app-test name in heroku>
  HEROKU_TEST_CONTAINER: registry.heroku.com/<app-test name in heroku>/web
  HEROKU_USER: <email or user_for_heroku_login>
```

Siendo:

* `web` un nombre cualquiera
