# Docker to AWS

Deploy api with docker into aws  EC2 or Lightsail

## GitLab Config for Deploy to AWS

![variables_aws](images/variables_aws.png)

```BASH
CONTAINER_NAME=<project_name_in_gitlab>-container
PORT_API=3000
PORT_EXPOSE=3000
PROJECT_PATH=<gitlab_user>/<project_name_in_gitlab>
SERVER_IP=<ip_server_of_aws>
SERVER_USER=ubuntu
SSH_KEY=-----BEGIN RSA PRIVATE KEY----- <continue>
```
