# GitLab-Pages-CI-CD

Contain a example of continuous deployment basic en GitLab Pages

## Creation Pipeline

create file `.gitlab-ci.yml` into add the code:

```YML
pages:
  stage: deploy
  script:
  - mkdir .public 
  - cp -r * .public
  - mv .public public 
  artifacts:
    paths:
    - public
  only:
  - master

```


## Pipeline
when doing push to master, the pipeline executing automatic and deploy in GitLab Pages

1. pipeline in progress after of push:

    ![pipeline-in-progress](images/pipe-in-progress.png)

2. pipeline after of execution successfully:

    ![pipeline](images/repo-pipe.png)


## GitLab Pages for Publish

1.  go to Settings - Pages

    ![page](images/pages.png)

2.  Copy the URL and paste in the Browser 

    ![link page](images/link-page.png)

3.  Result in Browser

    ![result](images/result.png)