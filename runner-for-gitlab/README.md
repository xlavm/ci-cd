# Create Runner in Gitlab

1. Create an instance of the GitLab Runner container.

    ```BASH
    docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:v15.5.1
    ```

2. Go to `Settings > CI/CD` and copy the URL and Token

    ![setting-ci/cd](images/1.png)

3. Register a Runner

    ```BASH
    docker exec -it gitlab-runner gitlab-runner register --docker-privileged
    ```

    * Specify `docker` as executor
    * Specify `docker:latest` as image

4. Restart runner

    ```BASH
    docker restart gitlab-runner
    ```

5. To list the configured runners

    ```BASH
    docker exec -it gitlab-runner gitlab-runner list
    ```

    output:

    ```BASH
    Runtime platform                 arch=amd64 os=linux pid=66 revision=775dd39d version=13.8.0
    Listing configured runners                         ConfigFile=/etc/gitlab-runner/config.toml
    a9a3dd7b82bd              Executor=docker Token=bxSoZytpPyWZLhB2MQQA URL=https://gitlab.com/
    9bad45a61010              Executor=docker Token=AQbMBaySx4qj8236uYFR URL=https://gitlab.com/
    ```

## Other commands

### Stopping, Starting and Restarting the Runners

* Stop

    ```BASH
    docker exec -it gitlab-runner gitlab-runner stop
    ```

* Start

    ```BASH
    docker exec -it gitlab-runner gitlab-runner start
    ```

* Restart

    ```BASH
    docker exec -it gitlab-runner gitlab-runner restart
    ```

### Deregister a Runner

```BASH
docker exec -it gitlab-runner gitlab-runner unregister --name a9a3dd7b82bd
```

If you have already removed the runner from the GitLab instance then that won’t work and you’ll get an error. However, this can be easily resolved using the following command.

```BASH
docker exec -it gitlab-runner gitlab-runner verify --delete
```

## Configuration File

The `config.toml` configuration file will be accessible at `/srv/gitlab-runner/config/config.toml` on the host machine.

![setting-ci/cd](images/2.png)
